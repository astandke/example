class FdsafsdfsController < ApplicationController
  before_action :set_fdsafsdf, only: [:show, :edit, :update, :destroy]

  # GET /fdsafsdfs
  # GET /fdsafsdfs.json
  def index
    @fdsafsdfs = Fdsafsdf.all
  end

  # GET /fdsafsdfs/1
  # GET /fdsafsdfs/1.json
  def show
  end

  # GET /fdsafsdfs/new
  def new
    @fdsafsdf = Fdsafsdf.new
  end

  # GET /fdsafsdfs/1/edit
  def edit
  end

  # POST /fdsafsdfs
  # POST /fdsafsdfs.json
  def create
    @fdsafsdf = Fdsafsdf.new(fdsafsdf_params)

    respond_to do |format|
      if @fdsafsdf.save
        format.html { redirect_to @fdsafsdf, notice: 'Fdsafsdf was successfully created.' }
        format.json { render :show, status: :created, location: @fdsafsdf }
      else
        format.html { render :new }
        format.json { render json: @fdsafsdf.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fdsafsdfs/1
  # PATCH/PUT /fdsafsdfs/1.json
  def update
    respond_to do |format|
      if @fdsafsdf.update(fdsafsdf_params)
        format.html { redirect_to @fdsafsdf, notice: 'Fdsafsdf was successfully updated.' }
        format.json { render :show, status: :ok, location: @fdsafsdf }
      else
        format.html { render :edit }
        format.json { render json: @fdsafsdf.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fdsafsdfs/1
  # DELETE /fdsafsdfs/1.json
  def destroy
    @fdsafsdf.destroy
    respond_to do |format|
      format.html { redirect_to fdsafsdfs_url, notice: 'Fdsafsdf was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fdsafsdf
      @fdsafsdf = Fdsafsdf.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fdsafsdf_params
      params[:fdsafsdf]
    end
end
