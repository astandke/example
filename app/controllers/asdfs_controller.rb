class AsdfsController < ApplicationController
  before_action :set_asdf, only: [:show, :edit, :update, :destroy]

  # GET /asdfs
  # GET /asdfs.json
  def index
    @asdfs = Asdf.all
  end

  # GET /asdfs/1
  # GET /asdfs/1.json
  def show
  end

  # GET /asdfs/new
  def new
    @asdf = Asdf.new
  end

  # GET /asdfs/1/edit
  def edit
  end

  # POST /asdfs
  # POST /asdfs.json
  def create
    @asdf = Asdf.new(asdf_params)

    respond_to do |format|
      if @asdf.save
        format.html { redirect_to @asdf, notice: 'Asdf was successfully created.' }
        format.json { render :show, status: :created, location: @asdf }
      else
        format.html { render :new }
        format.json { render json: @asdf.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /asdfs/1
  # PATCH/PUT /asdfs/1.json
  def update
    respond_to do |format|
      if @asdf.update(asdf_params)
        format.html { redirect_to @asdf, notice: 'Asdf was successfully updated.' }
        format.json { render :show, status: :ok, location: @asdf }
      else
        format.html { render :edit }
        format.json { render json: @asdf.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /asdfs/1
  # DELETE /asdfs/1.json
  def destroy
    @asdf.destroy
    respond_to do |format|
      format.html { redirect_to asdfs_url, notice: 'Asdf was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asdf
      @asdf = Asdf.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asdf_params
      params.require(:asdf).permit(:asdf)
    end
end
