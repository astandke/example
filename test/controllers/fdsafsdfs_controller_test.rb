require 'test_helper'

class FdsafsdfsControllerTest < ActionController::TestCase
  setup do
    @fdsafsdf = fdsafsdfs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fdsafsdfs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fdsafsdf" do
    assert_difference('Fdsafsdf.count') do
      post :create, fdsafsdf: {  }
    end

    assert_redirected_to fdsafsdf_path(assigns(:fdsafsdf))
  end

  test "should show fdsafsdf" do
    get :show, id: @fdsafsdf
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fdsafsdf
    assert_response :success
  end

  test "should update fdsafsdf" do
    patch :update, id: @fdsafsdf, fdsafsdf: {  }
    assert_redirected_to fdsafsdf_path(assigns(:fdsafsdf))
  end

  test "should destroy fdsafsdf" do
    assert_difference('Fdsafsdf.count', -1) do
      delete :destroy, id: @fdsafsdf
    end

    assert_redirected_to fdsafsdfs_path
  end
end
